/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Usuario;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author gustavoesteves
 */
public class usuarioDAO extends connectionBD{
  
    PreparedStatement pst;
    Statement st;
    ResultSet rs;
    
    boolean sucesso = false;
    
    public boolean inserirUsuario(Usuario novoUsuario){        
        //Conecta ao BD
        super.connectBD();
        sucesso = false;
        //Comando SQL
        String sql = "INSERT INTO Usuario values (?,?,?)";
        //O comando recebe paramêtros -> consulta dinâmica (pst)
        try {
            pst = super.con.prepareStatement(sql);
            pst.setString(1, novoUsuario.getEmail()); // 1 - refere-se à primeira interrogação
            pst.setString(2, novoUsuario.getNome_user()); // 2 - refere-se à segunda interrogação
            pst.setString(3, novoUsuario.getSenha()); // 3 - refere-se à terceira interrogação
            //pst.set<tipo da variavel>
            
            pst.execute();
            sucesso = true;
            
        } catch (SQLException ex) {
            msg.mostraMensagem("Erro: " + ex.getMessage());
            sucesso = false;
        } 
        finally{
            try {
                //Encerra a conexão
                super.con.close();
                pst.close();
            } catch (Exception ex) {
                msg.mostraMensagem("Erro ao fechar: " + ex.getMessage());
            }
        }
        return sucesso;
    }
    
    public boolean updateUsuario(String email, String nome){
        super.connectBD();
        sucesso = false;
        String sql = "UPDATE Usuario SET Nome_user=? WHERE Email=?";
        
        try {
            pst = super.con.prepareStatement(sql);
            pst.setString(1, nome);
            pst.setString(2, email);
            pst.execute();
            sucesso = true;
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
            sucesso = false;
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (SQLException e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
    
    public boolean deleteUsuario(String email){
        super.connectBD();
        sucesso = false;
        String sql = "DELETE FROM Usuario WHERE Email=?";
        
        try {
            pst = super.con.prepareStatement(sql);
            pst.setString(1, email);
            pst.execute();
            sucesso = true;
            
        } catch (SQLException e) {
          msg.mostraMensagem("Erro: " + e.getMessage());
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (Exception e) {
               msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
   
    public boolean verificarUsuarioAtivo(){
        super.connectBD();
        String aux;
        String sql = "SELECT * FROM Usuario";
        sucesso = false;
        try {
            st = super.con.createStatement();
            rs = st.executeQuery(sql);
            //System.out.println("Lista de Clãs");
            sucesso = rs.next();
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        
        finally{
            try {
                super.con.close();
                st.close();
            } catch (Exception e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
    
    public boolean buscarSenha(String email, String senha){
        super.connectBD();
        sucesso = false;
        String sql = "SELECT Senha FROM Usuario WHERE Email=?";
        String aux;
        try {
            pst = super.con.prepareStatement(sql);
            pst.setString(1,email);
            rs = pst.executeQuery();
            while (rs.next()) { 
                aux = rs.getString("Senha");
                if(aux.equals(senha)){
                    sucesso = true;
                }
                else sucesso = false;
            }
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (SQLException e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }            
        }
        return sucesso;
    }
    
    public boolean verificarEmailOcupado(String email){
        super.connectBD();
        sucesso = false;
        String sql = "SELECT Email FROM Usuario";
        try {
            st = super.con.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) { 
                String aux = rs.getString("Email");
                if(aux.equals(email))
                    sucesso = true;
            }            
        } catch (Exception e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        finally{
            try {
                super.con.close();
                st.close();
            } catch (SQLException ex) {
                msg.mostraMensagem("Erro ao Fechar: " + ex.getMessage());
            }
        }
        return sucesso;
    }
    
    public String buscarNomeUsuario(String email){
        super.connectBD();
        
        String sql = "SELECT Nome_user FROM Usuario WHERE Email=?";
        String nome = "";
        try {
            pst = super.con.prepareStatement(sql);
            pst.setString(1, email);             
            rs = pst.executeQuery();
            if(rs.next())
            nome = rs.getString("Nome_user");
            else msg.mostraMensagem("Erro Ao Achar Usuário");
                          
        } catch (Exception e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        finally{
            try {
                super.con.close();
                rs.close();
            } catch (SQLException ex) {
                msg.mostraMensagem("Erro ao Fechar: " + ex.getMessage());
            }
        }
        return nome;
    } 
}
