/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Cartas_has_Perfil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author gustavoesteves
 */
public class cartas_has_perfilDAO extends connectionBD{
    PreparedStatement pst;
    Statement st;
    ResultSet rs;
    
    boolean sucesso = false;
    
    public boolean insertCartahasPerfil(Cartas_has_Perfil novaCartaPerfil){        
        //Conecta ao BD
        super.connectBD();
        sucesso = false;
        //Comando SQL
        String sql = "INSERT INTO Cartas_has_Perfil(Cartas_idCarta, Perfil_idPerfil) values (?,?)";
        //O comando recebe paramêtros -> consulta dinâmica (pst)
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, novaCartaPerfil.getCartas_idCarta()); // 1 - refere-se à primeira interrogação
            pst.setInt(2, novaCartaPerfil.getPerfil_idPerfil()); // 2 - refere-se à segunda interrogação
            
            //pst.set<tipo da variavel>
            
            pst.execute();
            sucesso = true;
            
        } catch (SQLException ex) {
            msg.mostraMensagem("Erro: " + ex.getMessage());
            sucesso = false;
        } 
        finally{
            try {
                //Encerra a conexão
                super.con.close();
                pst.close();
            } catch (Exception ex) {
                msg.mostraMensagem("Erro ao fechar: " + ex.getMessage());
            }
        }
        return sucesso;
    }
    
    public boolean deleteCartaPerfil(int id){
        super.connectBD();
        sucesso = false;
        String sql = "DELETE FROM Cartas_has_Perfil WHERE Cartas_idCarta=?";
        
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, id);
            pst.execute();
            sucesso = true;
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
            sucesso = false;
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (SQLException e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
    
    public String[] buscarCartasPerfil(int id){
        String[] cartasPerfil = new String[1000];
        String aux;
        int x = 0;
        super.connectBD();        
        String sql = "SELECT idCarta, Nome_carta FROM Cartas, Cartas_has_Perfil " +
        "WHERE Cartas_idCarta = idCarta AND Perfil_idPerfil=? ORDER BY idCarta";
        cartasPerfil[0] = "Nenhuma Carta Cadastrada";        
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            while (rs.next()) {                
                aux = rs.getString("Nome_carta");
                if(aux!=null){
                    cartasPerfil[x] = "Id: " + rs.getInt("idCarta") + "  Nome: " + rs.getString("Nome_carta");
                    x ++;
                }
            }
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
            sucesso = false;
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (SQLException e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return cartasPerfil;
    }
}
