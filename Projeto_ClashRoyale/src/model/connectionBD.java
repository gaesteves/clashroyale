/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import view.MensagensTelas;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author gustavoesteves
 */
public class connectionBD {
    
    MensagensTelas msg = new MensagensTelas();
    //Objetos
    //Conexão com o servidor
    public Connection con;
//    //Prepara as consultas dinâmicas
//    PreparedStatement pst;
//    //Executa as consultas estáticas
//    Statement st;
//    //Referencia a tabela resultante da busca
//    ResultSet rs;
    
    //Nome BD
    String database = "ClashRoyale";
    //URL: verificar a porta
    String url = "jdbc:mysql://localhost:3306/" + database + "?useTimezone=true&serverTimezone=UTC&useSSl=false";
    //User
    String user = "root";
    //Password
    String password = "191919";
        
    //Conectar ao banco de dados
    public void connectBD(){
        try {
            con = DriverManager.getConnection(url, user, password);
            System.out.println("Conexão feita com sucesso");
        }
        catch (SQLException ex){
            msg.mostraMensagem("Erro: " + ex.getMessage());
        }
    }
}
