/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Perfil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author gustavoesteves
 */
public class perfilDAO extends connectionBD{
    
    PreparedStatement pst;
    Statement st;
    ResultSet rs;
    
    boolean sucesso = false;
    
    public boolean insertPerfil(Perfil novoPerfil){        
        //Conecta ao BD
        super.connectBD();
        sucesso = false;
        //Comando SQL
        String sql = "INSERT INTO Perfil(Nick,Idade,Usuario_Email,Clã_idClã) VALUES (?,?,?,?)";
        //O comando recebe paramêtros -> consulta dinâmica (pst)
        try {
            pst = super.con.prepareStatement(sql);
            pst.setString(1, novoPerfil.getNick()); 
            pst.setInt(2, novoPerfil.getIdade());
            pst.setString(3, novoPerfil.getUsuario_Email());
            pst.setInt(4, novoPerfil.getCla_idCla());
            
            pst.execute();
            sucesso = true;
            
        } catch (SQLException ex) {
            msg.mostraMensagem("Erro: " + ex.getMessage());
            sucesso = false;
        } 
        finally{
            try {
                //Encerra a conexão
                super.con.close();
                pst.close();
            } catch (Exception ex) {
                msg.mostraMensagem("Erro ao Fechar: " + ex.getMessage());
            }
        }
        return sucesso;
    }
    
    public boolean updatePerfil(String email, String nick, int id){
        super.connectBD();
        sucesso = false;
        String sql = "UPDATE Perfil SET Nick=?, Clã_idClã=? WHERE Usuario_Email=?";
        
        try {
            pst = super.con.prepareStatement(sql);
            pst.setString(1, nick);
            pst.setInt(2, id);
            pst.setString(3, email);
            pst.execute();
            sucesso = true;
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
            sucesso = false;
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (SQLException e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
    
    public boolean deletePerfil(Perfil deletePerfil){
        super.connectBD();
        sucesso = false;
        String sql = "DELETE FROM Perfil WHERE idPerfil=?";
        
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, deletePerfil.getIdPerfil());
            pst.execute();
            sucesso = true;
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Erro: " + e.getMessage());
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
       
    public int buscarIdPerfil(String email){
        super.connectBD();
        int perfil = 0, aux = 0;
        String sql = "SELECT idPerfil FROM Perfil WHERE Usuario_Email=?";
        try {
            pst = super.con.prepareStatement(sql);
            pst.setString(1, email);
            rs = pst.executeQuery();
            while (rs.next()) { 
                aux = rs.getInt("idPerfil");
                if(aux != 0){
                    perfil = aux;
                }                
            }
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (SQLException e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
            
        }
        return perfil;
    } 
    
    public Perfil buscarPerfil(int id){
        super.connectBD();
        Perfil perfil = new Perfil();
        String aux;
        String sql = "SELECT * FROM Perfil WHERE idPerfil=?";
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            while (rs.next()) { 
                aux = rs.getString("idPerfil");
                if(aux != null){
                    Perfil perfilTemp = new Perfil();
                    perfilTemp.perfil(rs.getString("Nick"),rs.getInt("idade"),
                                    rs.getString("Usuario_Email"),rs.getInt("Clã_idClã"));
                    perfilTemp.setIdPerfil(rs.getInt("idPerfil"));
                    perfil = perfilTemp;
                }                
            }
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
            sucesso = false;
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (SQLException e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
            
        }
        return perfil;
    }
}
