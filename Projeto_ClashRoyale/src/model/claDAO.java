/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Cla;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author gustavoesteves
 */
public class claDAO extends connectionBD{
    
    PreparedStatement pst;
    Statement st;
    ResultSet rs;
    
    boolean sucesso = false;
    
    public boolean insertCla(Cla novoCla){        
        //Conecta ao BD
        super.connectBD();
        sucesso = false;
        //Comando SQL
        String sql = "INSERT INTO Clã(idClã, Nome_cla, MinimoTrofeusParaEntrar,Localidade, Tipo) "
                    + "values (?,?,?,?,?)";
        //O comando recebe paramêtros -> consulta dinâmica (pst)
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, novoCla.getIdCla()); // 1 - refere-se à primeira interrogação
            pst.setString(2, novoCla.getNome_cla()); // 2 - refere-se à segunda interrogação
            pst.setInt(3, novoCla.getMinimoTrofeusParaEntrar()); // 3 - refere-se à terceira interrogação
            pst.setString(4, novoCla.getLocalidade());
            pst.setString(5, novoCla.getTipo());
            //pst.set<tipo da variavel>
            
            pst.execute();
            sucesso = true;
            
        } catch (SQLException ex) {
            msg.mostraMensagem("Erro: " + ex.getMessage());
        } 
        finally{
            try {
                //Encerra a conexão
                super.con.close();
                pst.close();
            } catch (Exception ex) {
                msg.mostraMensagem("Erro: " + ex.getMessage());
            }
        }
        return sucesso;
    }
        
    public boolean deleteCla(int id){
        super.connectBD();
        sucesso = false;
        String sql = "DELETE FROM Clã WHERE idClã=?";
        
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, id);
            pst.execute();
            sucesso = true;
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (Exception e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
   
    public boolean verificarCla(){
        super.connectBD();        
        String sql = "SELECT * FROM Clã";
        sucesso = false;
        try {
            st = super.con.createStatement();
            rs = st.executeQuery(sql);
            //System.out.println("Lista de Clãs");
            sucesso = rs.next();
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        
        finally{
            try {
                super.con.close();
                st.close();
            } catch (Exception e) {
                msg.mostraMensagem( "Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
    
    public boolean verificarClaExistente(int id){
        super.connectBD();        
        String sql = "SELECT * FROM Clã WHERE idClã=?";
        sucesso = false;
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            
            sucesso = rs.next();
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (Exception e) {
                msg.mostraMensagem( "Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
        
    public ArrayList<Cla> buscarAllCla(){
        ArrayList<Cla> listaCla = new ArrayList<>();
        super.connectBD();
        
        String sql = "SELECT * FROM Clã";
        
        try {
            st = super.con.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {                
                Cla claTemp = new Cla();
                claTemp.cla(rs.getInt("idClã"), rs.getString("Nome_cla"),rs.getInt("MinimoTrofeusParaEntrar"),
                        rs.getString("Localidade"),rs.getString("Tipo"));
                                
                listaCla.add(claTemp);
            }
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        
        finally{
            try {
                super.con.close();
                st.close();
            } catch (Exception e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return listaCla;
    }
}
