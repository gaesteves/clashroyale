/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.Cartas;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author gustavoesteves
 */
public class cartasDAO extends connectionBD{
    
    PreparedStatement pst;
    Statement st;
    ResultSet rs;
    
    boolean sucesso = false;
    
    public boolean insertCartas(Cartas novaCarta){        
        //Conecta ao BD
        super.connectBD();
        sucesso = false;
        //Comando SQL
        String sql = "INSERT INTO "
                   + "Cartas(Nome_carta, Raridade,Tipo,TotalVida,DanoAtaque,TempoAtaque,Alvos,Velocidade,Alcance)"
                   + " VALUES (?,?,?,?,?,?,?,?,?)";
        //O comando recebe paramêtros -> consulta dinâmica (pst)
        try {
            pst = super.con.prepareStatement(sql);
            pst.setString(1, novaCarta.getNome_carta());
            pst.setString(2, novaCarta.getRaridade()); 
            pst.setString(3, novaCarta.getTipo()); 
            pst.setInt(4, novaCarta.getTotalVida());
            pst.setInt(5, novaCarta.getDanoAtaque());
            pst.setFloat(6, novaCarta.getTempoAtaque());
            pst.setString(7, novaCarta.getAlvos());
            pst.setString(8, novaCarta.getVelocidade());
            pst.setFloat(9, novaCarta.getAlcance());
            //pst.set<tipo da variavel>
            
            pst.execute();
            sucesso = true;
            
        } catch (SQLException ex) {
            msg.mostraMensagem("Erro: " + ex.getMessage());
        } 
        finally{
            try {
                //Encerra a conexão
                super.con.close();
                pst.close();
            } catch (Exception ex) {
                msg.mostraMensagem("Erro ao fechar: " + ex.getMessage());
            }
        }
        return sucesso;
    }
        
    public boolean deleteCarta(int id){
        super.connectBD();
        sucesso = false;
        String sql = "DELETE FROM Cartas WHERE idCarta=?";
        
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, id);
            pst.execute();
            sucesso = true;
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (Exception e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
    
    public boolean verificarCartaExistente(int id){
        super.connectBD();        
        String sql = "SELECT * FROM Cartas WHERE idCarta=?";
        sucesso = false;
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            
            sucesso = rs.next();
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (Exception e) {
                msg.mostraMensagem( "Erro para fechar: " + e.getMessage());
            }
        }
        return sucesso;
    }
    
    public ArrayList<Cartas> buscarAllCartas(){
        ArrayList<Cartas> listaCartas = new ArrayList<>();
        super.connectBD();
        
        String sql = "SELECT * FROM Cartas ORDER BY idCarta";
        
        try {
            st = super.con.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {                
                Cartas cartaTemp = new Cartas();
                cartaTemp.cartas(rs.getString("Nome_carta"),rs.getString("Raridade"),
                        rs.getString("Tipo"),rs.getInt("TotalVida"),rs.getInt("DanoAtaque"),
                        rs.getFloat("TempoAtaque"), rs.getString("Alvos"), rs.getString("Velocidade"),
                        rs.getFloat("Alcance"));
                cartaTemp.setIdCartaAux(rs.getInt("idCarta"));
                
                listaCartas.add(cartaTemp);
            }
            
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        
        finally{
            try {
                super.con.close();
                st.close();
            } catch (Exception e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
        }
        return listaCartas;
    }
    
    public Cartas buscarCarta(int id){
        //ArrayList<Cartas> listaCartas = new ArrayList<>();
        super.connectBD();
        Cartas carta = null;
        String sql = "SELECT * FROM Cartas WHERE idCarta=?";
        String aux;
        try {
            pst = super.con.prepareStatement(sql);
            pst.setInt(1, id);
            rs = pst.executeQuery();
            while (rs.next()) { 
                aux = rs.getString("Nome_carta");
                if(aux != null){
                    Cartas cartaTemp = new Cartas();
                    cartaTemp.cartas(rs.getString("Nome_carta"),rs.getString("Raridade"),
                        rs.getString("Tipo"),rs.getInt("TotalVida"),rs.getInt("DanoAtaque"),
                        rs.getFloat("TempoAtaque"), rs.getString("Alvos"), rs.getString("Velocidade"),
                        rs.getFloat("Alcance"));
                    cartaTemp.setIdCartaAux(rs.getInt("idCarta"));
                    carta = cartaTemp;
                }                
            }
        } catch (SQLException e) {
            msg.mostraMensagem("Erro: " + e.getMessage());
        }
        finally{
            try {
                super.con.close();
                pst.close();
            } catch (SQLException e) {
                msg.mostraMensagem("Erro para fechar: " + e.getMessage());
            }
            
        }
        return carta ;
    }
    
}
