/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.controleDados;

/**
 *
 * @author gustavoesteves
 */
public class ClaTelas extends javax.swing.JFrame {

    controleDados controleDado = new controleDados();
    MensagensTelas msg = new MensagensTelas();
    
    private void clearTelas(){
        String[] clear = new String[0];
        clear[0] = "";
        lt_mostrarCla.setListData(clear);
    }
    
    private void listarAllCla(){
        lt_mostrarCla.setListData(controleDado.listarAllCla());
    }
    
    private void cadastrarCla(){
        String nome, localidade, tipo;
        int id, trofeus;
        
        nome = txt_nomeCla.getText();
        localidade = txt_localidade.getText();
        tipo = txt_tipo.getText();
        try {
            id = Integer.parseInt(txt_idCla.getText());
            try {
                trofeus = Integer.parseInt(txt_trofreus.getText());
                if(controleDado.cadastrarCla(id, nome, trofeus, localidade, tipo)) dispose();
            } catch (NumberFormatException e) {
                msg.mostraMensagem("Erro Trofeus: Permitido Somente Nº");
            }
        } catch (NumberFormatException e) {
            msg.mostraMensagem("Erro Id: Permitido Somente Nº");
        }
    }
    
    private void excluirCla(){
        int id;
        
        try {
            id = Integer.parseInt(txt_idClaExcluir.getText());
            if(controleDado.excluirCla(id)) dispose();
        } catch (NumberFormatException e) {
            msg.mostraMensagem("Erro Id Excluir: Permitido Somente Nº");
        }
    }
    
    /**
     * Creates new form Cla
     */
    public ClaTelas() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbl_idClaExcluir = new javax.swing.JLabel();
        txt_idClaExcluir = new javax.swing.JTextField();
        bt_lexcluirCla = new javax.swing.JButton();
        lbl_idCla = new javax.swing.JLabel();
        txt_idCla = new javax.swing.JTextField();
        lbl_nomeCla = new javax.swing.JLabel();
        txt_nomeCla = new javax.swing.JTextField();
        lbl_trofeus = new javax.swing.JLabel();
        txt_trofreus = new javax.swing.JTextField();
        lbl_localidade = new javax.swing.JLabel();
        txt_localidade = new javax.swing.JTextField();
        lbl_tipo = new javax.swing.JLabel();
        txt_tipo = new javax.swing.JTextField();
        bt_cadastrar = new javax.swing.JButton();
        bt_listar = new javax.swing.JButton();
        bt_voltar = new javax.swing.JButton();
        lt_mostrarCla = new javax.swing.JList<>();
        imagemFundo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbl_idClaExcluir.setBackground(new java.awt.Color(255, 255, 255));
        lbl_idClaExcluir.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 14)); // NOI18N
        lbl_idClaExcluir.setForeground(new java.awt.Color(255, 153, 51));
        lbl_idClaExcluir.setText("Nº Id Clã Excluir:");
        lbl_idClaExcluir.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_idClaExcluir.setOpaque(true);
        getContentPane().add(lbl_idClaExcluir, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 30, 140, -1));

        txt_idClaExcluir.setBackground(new java.awt.Color(255, 255, 255));
        txt_idClaExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idClaExcluirActionPerformed(evt);
            }
        });
        getContentPane().add(txt_idClaExcluir, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 50, 140, -1));

        bt_lexcluirCla.setBackground(new java.awt.Color(51, 51, 51));
        bt_lexcluirCla.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 18)); // NOI18N
        bt_lexcluirCla.setForeground(new java.awt.Color(255, 153, 51));
        bt_lexcluirCla.setText("Excluir Clã");
        bt_lexcluirCla.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white));
        bt_lexcluirCla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_lexcluirClaActionPerformed(evt);
            }
        });
        getContentPane().add(bt_lexcluirCla, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 80, 140, 34));

        lbl_idCla.setBackground(new java.awt.Color(255, 255, 255));
        lbl_idCla.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 14)); // NOI18N
        lbl_idCla.setForeground(new java.awt.Color(255, 153, 51));
        lbl_idCla.setText("Nº Id do Clã:");
        lbl_idCla.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_idCla.setOpaque(true);
        getContentPane().add(lbl_idCla, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, 120, -1));

        txt_idCla.setBackground(new java.awt.Color(255, 255, 255));
        txt_idCla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idClaActionPerformed(evt);
            }
        });
        getContentPane().add(txt_idCla, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 50, 230, -1));

        lbl_nomeCla.setBackground(new java.awt.Color(255, 255, 255));
        lbl_nomeCla.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 14)); // NOI18N
        lbl_nomeCla.setForeground(new java.awt.Color(255, 153, 51));
        lbl_nomeCla.setText("Nome do Clã:");
        lbl_nomeCla.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_nomeCla.setOpaque(true);
        getContentPane().add(lbl_nomeCla, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 100, 120, -1));

        txt_nomeCla.setBackground(new java.awt.Color(255, 255, 255));
        txt_nomeCla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_nomeClaActionPerformed(evt);
            }
        });
        getContentPane().add(txt_nomeCla, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 130, 230, -1));

        lbl_trofeus.setBackground(new java.awt.Color(255, 255, 255));
        lbl_trofeus.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 14)); // NOI18N
        lbl_trofeus.setForeground(new java.awt.Color(255, 153, 51));
        lbl_trofeus.setText("Mínimo Troféus:");
        lbl_trofeus.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_trofeus.setOpaque(true);
        getContentPane().add(lbl_trofeus, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 180, 120, -1));

        txt_trofreus.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(txt_trofreus, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 210, 230, -1));

        lbl_localidade.setBackground(new java.awt.Color(255, 255, 255));
        lbl_localidade.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 14)); // NOI18N
        lbl_localidade.setForeground(new java.awt.Color(255, 153, 51));
        lbl_localidade.setText("Localidade:");
        lbl_localidade.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_localidade.setOpaque(true);
        getContentPane().add(lbl_localidade, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 260, 120, -1));

        txt_localidade.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(txt_localidade, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 290, 230, -1));

        lbl_tipo.setBackground(new java.awt.Color(255, 255, 255));
        lbl_tipo.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 14)); // NOI18N
        lbl_tipo.setForeground(new java.awt.Color(255, 153, 51));
        lbl_tipo.setText("Tipo:");
        lbl_tipo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        lbl_tipo.setOpaque(true);
        getContentPane().add(lbl_tipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 340, 120, -1));

        txt_tipo.setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().add(txt_tipo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 370, 230, -1));

        bt_cadastrar.setBackground(new java.awt.Color(51, 51, 51));
        bt_cadastrar.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 18)); // NOI18N
        bt_cadastrar.setForeground(new java.awt.Color(255, 153, 51));
        bt_cadastrar.setText("Cadastrar");
        bt_cadastrar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white));
        bt_cadastrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_cadastrarActionPerformed(evt);
            }
        });
        getContentPane().add(bt_cadastrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 440, 150, 34));

        bt_listar.setBackground(new java.awt.Color(51, 51, 51));
        bt_listar.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 18)); // NOI18N
        bt_listar.setForeground(new java.awt.Color(255, 153, 51));
        bt_listar.setText("Listar Clãs");
        bt_listar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white));
        bt_listar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_listarActionPerformed(evt);
            }
        });
        getContentPane().add(bt_listar, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 390, 150, 34));

        bt_voltar.setBackground(new java.awt.Color(51, 51, 51));
        bt_voltar.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 18)); // NOI18N
        bt_voltar.setForeground(new java.awt.Color(255, 153, 51));
        bt_voltar.setText("Voltar");
        bt_voltar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white, java.awt.Color.white));
        bt_voltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt_voltarActionPerformed(evt);
            }
        });
        getContentPane().add(bt_voltar, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 440, 150, 34));

        lt_mostrarCla.setBackground(new java.awt.Color(255, 255, 255));
        lt_mostrarCla.setFont(new java.awt.Font("DejaVu Serif Condensed", 3, 12)); // NOI18N
        lt_mostrarCla.setForeground(new java.awt.Color(255, 153, 51));
        getContentPane().add(lt_mostrarCla, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 140, 340, 230));

        imagemFundo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cla.jpeg"))); // NOI18N
        getContentPane().add(imagemFundo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -10, -1, 500));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_idClaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idClaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idClaActionPerformed

    private void bt_cadastrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_cadastrarActionPerformed
        // TODO add your handling code here:
        cadastrarCla();
    }//GEN-LAST:event_bt_cadastrarActionPerformed

    private void bt_listarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_listarActionPerformed
        // TODO add your handling code here:
        listarAllCla();
    }//GEN-LAST:event_bt_listarActionPerformed

    private void bt_voltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_voltarActionPerformed
        // TODO add your handling code here:
        controleDado.voltar();
        dispose();
    }//GEN-LAST:event_bt_voltarActionPerformed

    private void txt_idClaExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idClaExcluirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idClaExcluirActionPerformed

    private void bt_lexcluirClaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt_lexcluirClaActionPerformed
        // TODO add your handling code here:
        excluirCla();
    }//GEN-LAST:event_bt_lexcluirClaActionPerformed

    private void txt_nomeClaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_nomeClaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_nomeClaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClaTelas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClaTelas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClaTelas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClaTelas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClaTelas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bt_cadastrar;
    private javax.swing.JButton bt_lexcluirCla;
    private javax.swing.JButton bt_listar;
    private javax.swing.JButton bt_voltar;
    private javax.swing.JLabel imagemFundo;
    private javax.swing.JLabel lbl_idCla;
    private javax.swing.JLabel lbl_idClaExcluir;
    private javax.swing.JLabel lbl_localidade;
    private javax.swing.JLabel lbl_nomeCla;
    private javax.swing.JLabel lbl_tipo;
    private javax.swing.JLabel lbl_trofeus;
    private javax.swing.JList<String> lt_mostrarCla;
    private javax.swing.JTextField txt_idCla;
    private javax.swing.JTextField txt_idClaExcluir;
    private javax.swing.JTextField txt_localidade;
    private javax.swing.JTextField txt_nomeCla;
    private javax.swing.JTextField txt_tipo;
    private javax.swing.JTextField txt_trofreus;
    // End of variables declaration//GEN-END:variables
}
