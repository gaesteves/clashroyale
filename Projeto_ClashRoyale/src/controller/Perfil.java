/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author gustavoesteves
 */
public class Perfil {
    private int idPerfil, Idade, Cla_idCla;
    private String Nick, Usuario_Email;
    
    public void perfil(String Nick, int Idade, String Usuario_Email, int Cla_idCla){
       this.Nick = Nick;
       this.Idade = Idade;
       this.Usuario_Email = Usuario_Email; 
       this.Cla_idCla = Cla_idCla;
    }

    public void setIdPerfil(int idPerfil) {
        this.idPerfil = idPerfil;
    }

    public int getIdPerfil() {
        return idPerfil;
    }

    public int getIdade() {
        return Idade;
    }        

    public int getCla_idCla() {
        return Cla_idCla;
    }

    public String getNick() {
        return Nick;
    }       

    public String getUsuario_Email() {
        return Usuario_Email;
    }
            
}
