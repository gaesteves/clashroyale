/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author gustavoesteves
 */
public class Cartas {
    private int idCarta, TotalVida, DanoAtaque;
    private String Nome_carta, Raridade, Tipo, Alvos, Velocidade;
    private float TempoAtaque, Alcance;
    
    public void cartas(String Nome_carta, String Raridade, String Tipo, int TotalVida,
                  int DanoAtaque, float TempoAtaque, String Alvos, String Velocidade,
                  float Alcance){
        this.Nome_carta = Nome_carta;
        this.Raridade = Raridade;
        this.Tipo = Tipo;
        this.TotalVida = TotalVida;
        this.DanoAtaque = DanoAtaque;
        this.TempoAtaque = TempoAtaque;
        this.Alvos = Alvos;
        this.Velocidade = Velocidade;
        this.Alcance = Alcance;
    }
    
    public int getIdCarta() {
        return idCarta;
    }
    
    public void setIdCartaAux(int id){
        this.idCarta = id;
    }

    public int getTotalVida() {
        return TotalVida;
    }
    public int getDanoAtaque() {
        return DanoAtaque;
    }
    
    public String getNome_carta() {
        return Nome_carta;
    }        

    public String getRaridade() {
        return Raridade;
    }        

    public String getTipo() {
        return Tipo;
    }
    public String getAlvos() {
        return Alvos;
    }

    public String getVelocidade() {
        return Velocidade;
    }

    public float getTempoAtaque() {
        return TempoAtaque;
    }
    public float getAlcance() {
        return Alcance;
    }
}
