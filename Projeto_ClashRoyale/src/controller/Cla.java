/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author gustavoesteves
 */
public class Cla {
    private int idCla, MinimoTrofeusParaEntrar;
    private String Nome_cla, Localidade, Tipo;
    
    public void cla(int idCla, String Nome_cla, int MinimoTrofeusParaEntrar, String Localidade, String Tipo){
        this.idCla = idCla;
        this.Nome_cla = Nome_cla;
        this.MinimoTrofeusParaEntrar = MinimoTrofeusParaEntrar;
        this.Localidade = Localidade;
        this.Tipo = Tipo;
    }
    
    public int getIdCla() {
        return idCla;
    }

    public void setIdCla(int idCla) {
        this.idCla = idCla;
    }   

    public int getMinimoTrofeusParaEntrar() {
        return MinimoTrofeusParaEntrar;
    }

    public String getNome_cla() {
        return Nome_cla;
    }

    public String getLocalidade() {
        return Localidade;
    }

    public String getTipo() {
        return Tipo;
    }
}
