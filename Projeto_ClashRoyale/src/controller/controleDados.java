/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.*;
import view.MensagensTelas;

/**
 *
 * @author gustavoesteves
 */
public class controleDados {
    boolean sucesso = false;
    //Mensagens
    MensagensTelas msg = new MensagensTelas();
    
    //Todas conexões com BD
    cartasDAO cartasBD = new cartasDAO();
    cartas_has_perfilDAO cartaPerfilBD = new cartas_has_perfilDAO();
    claDAO claBD = new claDAO();
    perfilDAO perfilBD = new perfilDAO();
    usuarioDAO usuarioBD = new usuarioDAO();
    
    //Telas
    controleTelas telas = new controleTelas();
    
    //Refente a Chamar Telas
    public void voltar(){
        telas.inicio();
    }
    
    public void cla(){
        telas.cla();
    }
    
    public void cartas(){
        telas.cartas();
    }
    
    //Referente a Usuário       
    public boolean usuarioExist(){
        sucesso = false;
        if(usuarioBD.verificarUsuarioAtivo()){
            telas.login();
            sucesso = true;            
        }
        else{
            msg.mostraMensagem("Nenhum Usuário Cadastrado");
            sucesso = false;
        }
        return sucesso;
    }
    
    public boolean cadastrarUsuario(String email, String nome, String senha, String nick,
                                    int idade, int idCla){
        sucesso = false;
        if(usuarioBD.verificarEmailOcupado(email)){
            msg.mostraMensagem("Email já Cadastrado");
            sucesso = false;
        }
        else {
            if (claBD.verificarClaExistente(idCla)) {
                Usuario novoUsuario = new Usuario();
                novoUsuario.usuario(email, nome, senha);                
                
                Perfil novoPerfil = new Perfil();
                novoPerfil.perfil(nick, idade, email, idCla);
                
                if(usuarioBD.inserirUsuario(novoUsuario) && 
                        perfilBD.insertPerfil(novoPerfil)){
                    msg.mostraMensagem("Cadastro Realizado");
                    sucesso = true;
                    telas.cadastrarUsuario();
                }
            }
            else  msg.mostraMensagem("Id do Clã Não Existente");
        }
        return sucesso;
    }
    
    public boolean verificarLogin(String email, String senha){
        sucesso = false;
        if(usuarioBD.verificarEmailOcupado(email)){
            if(usuarioBD.buscarSenha(email, senha)) {
                msg.mostraMensagem("Login Realizado");
                sucesso = true;
            }
            else msg.mostraMensagem("Senha Incorreta");
        }
        else msg.mostraMensagem("Email Incorreto");
        return sucesso;
    }
    
    public boolean alterarUsuario(String email, String nome, String nick, int id){
        if(claBD.verificarClaExistente(id))
            if(usuarioBD.updateUsuario(email, nome) && perfilBD.updatePerfil(email, nick, id)) sucesso = true;
            else sucesso = false;
        else msg.mostraMensagem("Id Novo Clã Não Existe");
        
        return sucesso;
    }
    
    public boolean excluirUsuario(String email){
        sucesso = false;
        if(usuarioBD.deleteUsuario(email)){
            msg.mostraMensagem("Usuário Excluído Com Sucesso");
            telas.inicio();
            sucesso = true;
        }
        else msg.mostraMensagem("Erro Ao Excluir Usuário");
        return sucesso;
    }
    
    //Referente a Perfil
    public int getIdPerfil(String email){
        int idPerfil = 0;
        idPerfil = perfilBD.buscarIdPerfil(email);
        return idPerfil;
    }
    
    public void adicionarCarta(int idPerfil, int idCarta){
        Cartas_has_Perfil cartaPerfil = new Cartas_has_Perfil();
        
        if(cartasBD.verificarCartaExistente(idCarta)){
            cartaPerfil.cartas_has_Perfil(idCarta, idPerfil);
            if(cartaPerfilBD.insertCartahasPerfil(cartaPerfil)) msg.mostraMensagem("Adicionada Com Sucesso");
            else msg.mostraMensagem("Erro Ao Adicionar");
        }
        else msg.mostraMensagem("Id Carta Não Existente");
        
    }
    
    public String[] buscarCartasPerfil(int id){
        String[] cartasPerfil = new String[1000];
        cartasPerfil = cartaPerfilBD.buscarCartasPerfil(id);
        return cartasPerfil;
    }
    
    public String[] buscarPerfil(String email, int id){
        Perfil auxPerfil = new Perfil();
        String[] perfil = new String[6];
        auxPerfil = perfilBD.buscarPerfil(id);
        perfil[0] = "Nome Usuário: " + usuarioBD.buscarNomeUsuario(email);
        perfil[1] = "Id Perfil: " + auxPerfil.getIdPerfil();
        perfil[2] = "Nick: " + auxPerfil.getNick();
        perfil[3] = "Idade: " + auxPerfil.getIdade();
        perfil[4] = "Email: " + auxPerfil.getUsuario_Email();
        perfil[5] = "Id Do Clã: " + auxPerfil.getCla_idCla();
        
        return perfil;
    }
    
    public void retirarCarta(int id){
        if(cartasBD.verificarCartaExistente(id))
            if(cartaPerfilBD.deleteCartaPerfil(id)) msg.mostraMensagem("Carta Retirada Com Sucesso");
            else msg.mostraMensagem("Erro Para Retirar Carta");
        else msg.mostraMensagem("Id Carta Não Existente");
    }
    
    //Referente a Clã
    public boolean claExist(){
        sucesso = false;
        if (claBD.verificarCla()) {
            telas.cadastrarUsuario();
            sucesso = true;
        }
        else msg.mostraMensagem("Nenhum Clã Cadastrado");
        return sucesso;
    }
    
    public String[] listarAllCla(){
        ArrayList<Cla> allCla = new ArrayList<>();
        allCla = claBD.buscarAllCla();
        
        int tam = 1;
        if(allCla.isEmpty()) tam = 1; else tam = allCla.size();
        String[] listaAllCla = new String[tam];
        
        if(!allCla.isEmpty())
            for(int i = 0; i < listaAllCla.length; i++){
                listaAllCla[i] = allCla.get(i).getIdCla() +"- " + allCla.get(i).getNome_cla()+
                                " MinTrofeus:"+allCla.get(i).getMinimoTrofeusParaEntrar()+
                                " Local:"+allCla.get(i).getLocalidade() + " Tipo:"+allCla.get(i).getTipo();
            }           
        else listaAllCla[0] = "Nenhum Clã Cadastrado";
        return listaAllCla;
    }
    
    public boolean cadastrarCla(int id, String nome, int trofeus, String localidade, String tipo){
        sucesso = false;
        Cla novoCla = new Cla();       
        novoCla.cla(id, nome, trofeus, localidade, tipo);
        
        if(claBD.verificarClaExistente(id)) msg.mostraMensagem("Id Já Utilizado Por Outro Clã");
        else{
            if(claBD.insertCla(novoCla)) {
                msg.mostraMensagem("Cadastro Realizado");
                telas.cla();
                sucesso = true;
            }
            else msg.mostraMensagem("Erro Para Cadastrar");
        }
        return sucesso;
    }
    
    public boolean excluirCla(int id){
        sucesso = false;
        if(claBD.verificarClaExistente(id)){
            if(claBD.deleteCla(id)) {
                msg.mostraMensagem("Excluído Com Sucesso");
                telas.cla();
                sucesso = true;
            }
            else msg.mostraMensagem("Erro Para Excluir(Verifique Seu ID)");
        
        }
        else msg.mostraMensagem("Id Não Existente");
        return sucesso;        
    }
    
        
    //Referente a Carta
    public boolean cadastrarCarta(String nome, String rariridade, String tipo, String alvos, String velocidade,
                                int totalVida, int danoAtaque,float tempoAtaque, float alcance){
        sucesso = false;
        Cartas novaCarta = new Cartas();
        novaCarta.cartas(nome, rariridade, tipo, totalVida, danoAtaque, tempoAtaque, alvos, velocidade, alcance);
        if(cartasBD.insertCartas(novaCarta)) {
            msg.mostraMensagem("Cadastro Realizado");
            telas.cartas();
            sucesso = true;
        }
        else msg.mostraMensagem("Erro Cadastro Carta Nova");
        return sucesso;       
    }
    
    public String[] listarCarta(int id){   
        Cartas ct = new Cartas();
               
        ct = cartasBD.buscarCarta(id);
        
        String[] listarCarta = new String[11];
        
        if(ct != null) {
            listarCarta[0] = "Id Carta: " + ct.getIdCarta();
            listarCarta[1] = "Nome: " + ct.getNome_carta();
            listarCarta[2] = "Raridade: " + ct.getRaridade();
            listarCarta[4] = "Tipo: " + ct.getTipo();
            listarCarta[5] = "Total Vida(pts): " + ct.getTotalVida();
            listarCarta[6] = "Dano Ataque(pts): " + ct.getDanoAtaque();
            listarCarta[7] = "Tempo Ataque(s): " + ct.getTempoAtaque();
            listarCarta[8] = "Alvos: " + ct.getAlvos();
            listarCarta[9] = "Velocidade: " + ct.getVelocidade();
            listarCarta[10] = "Alcance: " + ct.getAlcance();
        }
        else listarCarta[0] = "Id Incorreto";
        
        return listarCarta;
    }
    
    public boolean excluirCarta(int id){
        sucesso = false;
        if(cartasBD.verificarCartaExistente(id))
            if (cartasBD.deleteCarta(id)) {
                msg.mostraMensagem("Excluída Com Sucesso");
                sucesso = true;
                telas.cartas();
            }
            else msg.mostraMensagem("Erro Para Excluir(Verifique Seu ID)");
        else msg.mostraMensagem("Carta Não Existe Para Ser Excluída");
        return sucesso;
    }
    
    public String[] listarAllCartas(){
        ArrayList<Cartas> allCartas = new ArrayList<>();
        allCartas = cartasBD.buscarAllCartas();
        
        int tam = 1;
        if(allCartas.isEmpty()) tam = 1; 
        else tam = allCartas.size();
        String[] listaAllCartas = new String[tam];
        
        if(!allCartas.isEmpty())
            for(int i = 0; i < listaAllCartas.length; i++){
                listaAllCartas[i] = "Id: " + allCartas.get(i).getIdCarta() +" - Nome: " + allCartas.get(i).getNome_carta();
            }           
        else listaAllCartas[0] = "Nenhuma Carta Cadastrada";
        return listaAllCartas;
    }
    
}
