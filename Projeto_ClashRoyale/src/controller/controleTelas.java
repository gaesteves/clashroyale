/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import view.*;

/**
 *
 * @author gustavoesteves
 */
public class controleTelas {
    
    public void inicio(){
        InicioTelas inicio = new InicioTelas();
        inicio.setVisible(true);
    }
    
    public void login(){    
        LoginTelas login = new LoginTelas();
        login.setVisible(true);
    }
    
    public void usuario(){
        UsuarioTelas usuario = new UsuarioTelas();
        usuario.setVisible(true);
    }
    
    public void cla(){
        ClaTelas cla = new ClaTelas();
        cla.setVisible(true);
    }
    
    public void cartas(){
        CartasTelas cartas = new CartasTelas();
        cartas.setVisible(true);
    }
    
    public void cadastrarUsuario(){
        CadastrarUsuarioTelas cadastrausuario = new CadastrarUsuarioTelas();
        cadastrausuario.setVisible(true);
    }
}
