-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `ClashRoyale` DEFAULT CHARACTER SET utf8 ;
USE `ClashRoyale` ;

CREATE TABLE IF NOT EXISTS `ClashRoyale`.`Usuario` (
  `Email` VARCHAR(80) NOT NULL,
  `Nome_user` VARCHAR(45) NOT NULL,
  `Senha` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Email`),
  UNIQUE INDEX `Email_UNIQUE` (`Email` ASC),
  UNIQUE INDEX `Nome_user_UNIQUE` (`Nome_user` ASC))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `ClashRoyale`.`Clã` (
  `idClã` INT NOT NULL ,
  `Nome_cla` VARCHAR(45) NOT NULL,
  `MinimoTrofeusParaEntrar` INT NOT NULL,
  `Localidade` VARCHAR(20) NOT NULL,
  `Tipo` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`idClã`),
  UNIQUE INDEX `idClã_UNIQUE` (`idClã` ASC),
  UNIQUE INDEX `Nome_cla_UNIQUE` (`Nome_cla` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ClashRoyale`.`Perfil` (
  `idPerfil` INT NOT NULL AUTO_INCREMENT,
  `Nick` VARCHAR(45) NOT NULL,
  `Idade` INT NOT NULL,
  `Usuario_Email` VARCHAR(80) NOT NULL,
  `Clã_idClã` INT NOT NULL,
  PRIMARY KEY (`idPerfil`),
  UNIQUE INDEX `idPerfil_UNIQUE` (`idPerfil` ASC),
  UNIQUE INDEX `Nick_UNIQUE` (`Nick` ASC),
  INDEX `fk_Perfil_Usuario_idx` (`Usuario_Email` ASC),
  INDEX `fk_Perfil_Clã1_idx` (`Clã_idClã` ASC),
  CONSTRAINT `fk_Perfil_Usuario`
    FOREIGN KEY (`Usuario_Email`)
    REFERENCES `ClashRoyale`.`Usuario` (`Email`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Perfil_Clã1`
    FOREIGN KEY (`Clã_idClã`)
    REFERENCES `ClashRoyale`.`Clã` (`idClã`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ClashRoyale`.`Cartas` (
  `idCarta` INT NOT NULL AUTO_INCREMENT,
  `Nome_carta` VARCHAR(45) NOT NULL,
  `Raridade` VARCHAR(45) NOT NULL,
  `Tipo` VARCHAR(45) NOT NULL,
  `TotalVida` INT NOT NULL,
  `DanoAtaque` INT NOT NULL,
  `TempoAtaque` FLOAT NOT NULL,
  `Alvos` VARCHAR(45) NOT NULL,
  `Velocidade` VARCHAR(45) NOT NULL,
  `Alcance` FLOAT NOT NULL,
  PRIMARY KEY (`idCarta`),
  UNIQUE INDEX `idCarta_UNIQUE` (`idCarta` ASC),
  UNIQUE INDEX `Nome_carta_UNIQUE` (`Nome_carta` ASC))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `ClashRoyale`.`Cartas_has_Perfil` (
  `Cartas_idCarta` INT NOT NULL,
  `Perfil_idPerfil` INT NOT NULL,
  PRIMARY KEY (`Cartas_idCarta`, `Perfil_idPerfil`),
  INDEX `fk_Cartas_has_Perfil_Perfil1_idx` (`Perfil_idPerfil` ASC),
  INDEX `fk_Cartas_has_Perfil_Cartas1_idx` (`Cartas_idCarta` ASC),
  CONSTRAINT `fk_Cartas_has_Perfil_Cartas1`
    FOREIGN KEY (`Cartas_idCarta`)
    REFERENCES `ClashRoyale`.`Cartas` (`idCarta`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Cartas_has_Perfil_Perfil1`
    FOREIGN KEY (`Perfil_idPerfil`)
    REFERENCES `ClashRoyale`.`Perfil` (`idPerfil`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
